import { Action } from 'redux';

import { actionTypes } from '../action-types';
import { Article } from '../models/article';
import { BlockProduct } from '../models/block-product';
import { BlockText } from '../models/block-text';

export interface ArticleAction extends Action {
  article?: Article;
  articleItemIndex?: number;
  articleItem?: (BlockText|BlockProduct);
}

export const setCurrentArticle: (article: Article) => ArticleAction =
  (article) => {
    return {
      type: actionTypes.SET_ARTICLE,
      article,
    };
  };

export const addNewTextBlock: () => Action =
  () => {
    return {
      type: actionTypes.ADD_NEW_TEXT_BLOCK,
    };
  };

export const addNewProductBlock: () => Action =
  () => {
    return {
      type: actionTypes.ADD_NEW_PRODUCT_BLOCK,
    };
  };

export const deleteBlock: (articleItemIndex: number) => ArticleAction =
  (articleItemIndex) => {
    return {
      type: actionTypes.DELETE_BLOCK,
      articleItemIndex
    };
  };

export const setArticleItemToEdit:
  (articleItemIndex: number, articleItem: (BlockText|BlockProduct)) => ArticleAction =
    (articleItemIndex, articleItem) => {
      return {
        type: actionTypes.SET_ARTICLE_ITEM_TO_EDIT,
        articleItemIndex,
        articleItem,
      };
    };

export const updateEditedArticle: (articleItem: (BlockText|BlockProduct)) => ArticleAction =
  (articleItem) => {
    return {
      type: actionTypes.UPDATE_EDITED_ARTICLE,
      articleItem: articleItem,
    };
  };

export const cancelEditArticle: () => Action =
  () => {
    return {
      type: actionTypes.CANCEL_EDIT_ARTICLE
    };
  };

export const clearCurrentArticle: () => Action =
  () => {
    return {
      type: actionTypes.CLEAR_ARTICLE,
    };
  };
