import { Action } from 'redux';
import { actionTypes } from '../action-types';
import { Product } from '../models/product';

export interface ProductAction extends Action {
  products: Product[];
}

export const setProducts: (products: Product[]) => ProductAction =
  (products) => {
  return {
      type: actionTypes.SET_PRODUCTS,
      products,
  };
};

export const clearProducts: () => Action =
  () => {
  return {
      type: actionTypes.CLEAR_PRODUCTS,
  };
};
