import * as React from 'react';
import { connect } from 'react-redux';
import { getProducts } from './models/product';
import { Article } from './models/article';
import { isBlockText } from './models/block-text';
import { isBlockProduct } from './models/block-product';

import { setProducts } from './actions/product-actions';
import { Dispatch } from 'redux';
import { AppState } from './app-state';

import AddContentBlock from './components/blocks/AddContentBlock';
import ProductBlock from './components/blocks/ProductBlock';
import TextBlock from './components/blocks/TextBlock';

import EditDropDown from './components/EditDropDown';

interface AppProps {
  article: Article;
  dispatch: Dispatch<AppState>;
}

class App extends React.Component<AppProps, {}> {

  constructor(props: AppProps) {
    super(props);

    this.getArticleBlocks = this.getArticleBlocks.bind(this);
    this.exportJSON = this.exportJSON.bind(this);

    // Initialise the app
    const products = getProducts();
    this.props.dispatch(setProducts(products));
  }

  public getArticleBlocks() {
    return this.props.article.items.map(
      (articleItem, index) => {
        if (isBlockText(articleItem)) {
          return <TextBlock item={articleItem} itemIndex={index} key={index} />;
        }
        if (isBlockProduct(articleItem)) {
          return <ProductBlock item={articleItem} itemIndex={index} key={index} />;
        }
        return null;
      }
    );
  }

  public render() {
    const articleBlocks = this.getArticleBlocks();

    return (
      <div className="v-app">
        <div className="o-layout-fixed-width">
          <div className="v-app__export-json" onClick={this.exportJSON}>Export json</div>
          <h1 className="v-app__title">Article Editor</h1>

          {articleBlocks}

          <AddContentBlock />
        </div>
        <EditDropDown />
      </div>
    );
  }

  private exportJSON() {
    alert(JSON.stringify(this.props.article.items, null, '\t'));
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    article: state.article.currentArticle,
  };
};

export default connect(mapStateToProps)(App);
