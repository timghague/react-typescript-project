import { Product } from "./models/product";
import { Article } from "./models/article";
import { BlockProduct } from './models/block-product';
import { BlockText } from './models/block-text';

export interface ProductState {
	allProducts: Product[];
}

export interface ArticleState {
	currentArticle: Article;
	editArticleIndex: (number|boolean);
	editArticleItem?: (BlockProduct|BlockText);
}

export interface AppState {
	product: ProductState
	article: ArticleState;
}
