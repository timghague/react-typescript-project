import {BlockProduct} from './block-product'
import {BlockText} from './block-text'

export class Article {
	public id: number;
	public items: (BlockText|BlockProduct)[];
};

const articles: Article[] = [{
  id: 1,
  items: [
    {
      "type": "text",
      "title": "This is a title",
      "body": "This is body text"
    },
    {
      "type": "products",
      "products": [167687, 168012]
    },
    {
      "type": "text",
      "title": "Title of final text block",
      "body": "This is body text again"
    }
  ]
}]

export const getArticle = (articleId: number) =>
  articles.find(article => article.id === articleId)
