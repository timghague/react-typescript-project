export class BlockProduct {
  public type: string;
  public products: number[];
}

export const getNewProductBlock = () => ({
  'type': 'products',
  'products': [],
});

export const isBlockProduct = (obj: any): obj is BlockProduct => {
  return obj.type === 'products';
};
