export class BlockText {
  public type: string;
  public title: string;
  public body: string;
}

export const getNewTextBlock = () => ({
  type: 'text',
  title: '',
  body: ''
});

export const isBlockText = (obj: any): obj is BlockText => {
  return obj.type === 'text';
};
