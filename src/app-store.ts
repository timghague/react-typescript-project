import { createStore } from 'redux';
import reducer from './reducers';

export const appStore = createStore(reducer);
