import * as React from 'react';
import { connect } from 'react-redux';
import { BlockProduct, isBlockProduct } from '../models/block-product';
import { BlockText, isBlockText } from '../models/block-text';
import { AppState } from '../app-state';
import { Dispatch } from 'redux';

import { cancelEditArticle } from '../actions/article-actions';

import EditProductBlock from './EditProductBlock';
import EditTextBlock from './EditTextBlock';

interface EditDropDownProps {
  editArticleIndex: (number|boolean);
  editArticleItem: (BlockProduct|BlockText);
  dispatch: Dispatch<AppState>;
}

interface EditDropDownState {
  animateIn: boolean;
}

class EditDropDown extends React.Component<EditDropDownProps, EditDropDownState> {

  constructor(props: EditDropDownProps) {
    super(props);

    this.cancelEdit = this.cancelEdit.bind(this);

    this.state = {
      animateIn: false,
    };
  }

  public render() {
    if (this.props.editArticleIndex === false) {
      return null;
    }

    let editUI = null;
    if (isBlockText(this.props.editArticleItem)) {
      editUI = <EditTextBlock item={this.props.editArticleItem} />;
    }
    if (isBlockProduct(this.props.editArticleItem)) {
      editUI = <EditProductBlock />;
    }

    return (
      <div
        className={
          'c-edit-drop-down ' + (this.state.animateIn ? 'c-edit-drop-down__animate-in' : '')
        }
      >
        <div className="c-edit-drop-down__mask" onClick={this.cancelEdit} />

        <div className="c-edit-drop-down__edit-area">
          <div className="c-edit-drop-down__edit-area-inner">
            {editUI}
          </div>
        </div>

      </div>
    );
  }

  public componentDidUpdate(prevProps: EditDropDownProps) {
    if (prevProps.editArticleIndex === false && this.props.editArticleIndex !== false) {
      this.setState(
        { animateIn: false },
        () => {
          setTimeout(
            () => {
              this.setState({
                animateIn: true
              });
            },
            50 // Delay to ensure animate class cleared
          );
        }
      );
    }
  }

  private cancelEdit() {
    this.props.dispatch(cancelEditArticle());
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    editArticleIndex: state.article.editArticleIndex,
    editArticleItem: state.article.editArticleItem,
  };
};

export default connect(mapStateToProps)(EditDropDown);
