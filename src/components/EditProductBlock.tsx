import * as React from 'react';
import { connect } from 'react-redux';
import { Product } from '../models/product';
import { AppState } from '../app-state';

interface EditProductBlockProps {
  products: Product[];
}

class EditProductBlock extends React.Component<EditProductBlockProps, {}> {
  public render() {
    return (
      <div className="c-edit-product-block">
        Edit product block
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    products: state.product.allProducts
  };
};

export default connect(mapStateToProps)(EditProductBlock);
