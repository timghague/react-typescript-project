import * as React from 'react';
import { connect } from 'react-redux';
import { BlockText } from '../models/block-text';
import { AppState } from '../app-state';
import { Dispatch } from 'redux';

import { updateEditedArticle, cancelEditArticle } from '../actions/article-actions';

interface EditTextBlockProps {
  item: BlockText;
  dispatch: Dispatch<AppState>;
}

interface EditTextBlockState {
  title: string;
  body: string;
}

class EditTextBlock extends React.Component<EditTextBlockProps, EditTextBlockState> {

  constructor(props: EditTextBlockProps) {
    super(props);

    this.onTitleInputChange = this.onTitleInputChange.bind(this);
    this.onBodyInputChange = this.onBodyInputChange.bind(this);
    this.saveBlock = this.saveBlock.bind(this);
    this.cancelEdit = this.cancelEdit.bind(this);
    this.getTextBlockItem = this.getTextBlockItem.bind(this);

    this.state = {
      title: this.props.item.title,
      body: this.props.item.body,
    };
  }

  public render() {
    return (
      <div className="c-edit-text-block">
        <div className="c-edit-text-block__top-section">
          <h3 className="c-edit-text-block__title">Editing text</h3>
        </div>
        <div className="c-edit-text-block__form">
          <input
            className="c-edit-text-block__text-input"
            type="text"
            placeholder="Title"
            value={this.state.title}
            onChange={this.onTitleInputChange}
          />
          <textarea
            className="c-edit-text-block__textarea"
            placeholder="Body"
            value={this.state.body}
            onChange={this.onBodyInputChange}
          />

          <div className="c-edit-text-block__buttons">
            <button onClick={this.cancelEdit} className="c-btn c-btn--secondary c-edit-text-block__button">
              Cancel
            </button>
            <button onClick={this.saveBlock} className="c-btn c-btn--primary c-edit-text-block__button">
              Save Changes
            </button>
          </div>
        </div>
      </div>
    );
  }

  private getTextBlockItem() {
    return {
      type: 'text',
      title: this.state.title,
      body: this.state.body,
    };
  }

  private saveBlock() {
    this.props.dispatch(updateEditedArticle(this.getTextBlockItem()));
  }

  private cancelEdit() {
    this.props.dispatch(cancelEditArticle());
  }

  private onTitleInputChange(e: any) {
    const title = e.target.value;
    this.setState({
      title
    });
  }

  private onBodyInputChange(e: any) {
    const body = e.target.value;
    this.setState({
      body
    });
  }
}

export default connect()(EditTextBlock);
