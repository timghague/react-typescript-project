import * as React from 'react';
import { connect } from 'react-redux';

import { Dispatch } from 'redux';
import { AppState } from '../../app-state';
import { BlockProduct } from '../../models/block-product';

import { setArticleItemToEdit, deleteBlock } from '../../actions/article-actions';

interface ProductBlockProps {
  item: BlockProduct;
  itemIndex: number;
  dispatch: Dispatch<AppState>;
}

class ProductBlock extends React.Component<ProductBlockProps, {}> {

  constructor(props: ProductBlockProps) {
    super(props);

    this.isFilled = this.isFilled.bind(this);
    this.editBlock = this.editBlock.bind(this);
    this.deleteBlock = this.deleteBlock.bind(this);
    this.getItemData = this.getItemData.bind(this);
  }

  public render() {
    return (
      <div className="c-product-block">
        <div className="c-product-block__top-section">
          <h3 className="c-product-block__title">
            Products
          </h3>

          <div className="c-product-block__buttons">
            <div onClick={this.editBlock} className="c-product-block__button">
              Edit
            </div>
            <div onClick={this.deleteBlock} className="c-product-block__button">
              Delete
            </div>
          </div>
        </div>

        <div className="c-product-block__content">
          {(
            this.isFilled() ?
              <div onClick={this.editBlock} className="c-product-block__content-no-products">
                <span className="c-product-block__content-no-products-inner">
                  No products selected.
                </span>
              </div> :
              <div className="c-product-block__content-has-products" />
          )}
        </div>
      </div>
    );
  }

  private isFilled() {
    if (this.props.item.products.length > 0) {
      return false;
    }
    return true;
  }

  private getItemData() {
    return {
      type: 'products',
      products: this.props.item.products
    };
  }

  private editBlock() {
    this.props.dispatch(
      setArticleItemToEdit(this.props.itemIndex, this.getItemData())
    );
  }

  private deleteBlock() {
    this.props.dispatch(
      deleteBlock(this.props.itemIndex)
    );
  }
}

export default connect()(ProductBlock);
