import * as React from 'react';
import { connect } from 'react-redux';

import { Dispatch } from 'redux';
import { AppState } from '../../app-state';
import { BlockText } from '../../models/block-text';

import { setArticleItemToEdit, deleteBlock } from '../../actions/article-actions';

interface TextBlockProps {
  item: BlockText;
  itemIndex: number;
  dispatch: Dispatch<AppState>;
}

class TextBlock extends React.Component<TextBlockProps, {}> {
    constructor(props: TextBlockProps) {
      super(props);

      this.isFilled = this.isFilled.bind(this);
      this.editBlock = this.editBlock.bind(this);
      this.deleteBlock = this.deleteBlock.bind(this);
      this.getItemData = this.getItemData.bind(this);
    }

    public render() {
      return (
        <div className="c-text-block">
          <div className="c-text-block__top-section">
            <h3 className="c-text-block__title">
              Text
            </h3>

            <div className="c-text-block__buttons">
              <div onClick={this.editBlock} className="c-text-block__button">
                Edit
              </div>
              <div onClick={this.deleteBlock} className="c-text-block__button">
                Delete
              </div>
            </div>
          </div>
          <div className="c-text-block__content">
            {(
              this.isFilled() ?
              <div onClick={this.editBlock} className="c-text-block__content-no-text">
                <span className="c-text-block__content-no-text-inner">
                  No text added.
                </span>
              </div> :
              <div className="c-text-block__content-has-text">
                <div className="c-text-block__content-has-text-title">
                  {this.props.item.title}
                </div>
                <div className="c-text-block__content-has-text-body">
                  {this.props.item.body}
                </div>
              </div>
            )}
          </div>
        </div>
      );
    }

    private isFilled() {
      if (this.props.item.title.length > 0 || this.props.item.body.length > 0) {
        return false;
      }
      return true;
    }

    private getItemData() {
      return {
        type: 'text',
        title: this.props.item.title,
        body: this.props.item.body
      };
    }

    private editBlock() {
      this.props.dispatch(
        setArticleItemToEdit(this.props.itemIndex, this.getItemData())
      );
    }

    private deleteBlock() {
      this.props.dispatch(
        deleteBlock(this.props.itemIndex)
      );
    }
}

export default connect()(TextBlock);
