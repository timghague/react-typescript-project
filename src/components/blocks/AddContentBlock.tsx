import * as React from 'react';
import { connect } from 'react-redux';

import { Dispatch } from 'redux';
import { AppState } from '../../app-state';

import { addNewTextBlock, addNewProductBlock } from '../../actions/article-actions';

interface AddContentBlockProps {
  dispatch: Dispatch<AppState>;
}

class AddContentBlock extends React.Component<AddContentBlockProps, {}> {

  constructor(props: AddContentBlockProps) {
    super(props);
    this.addTextBlock = this.addTextBlock.bind(this);
    this.addProductBlock = this.addProductBlock.bind(this);
  }

  public addTextBlock() {
    this.props.dispatch(addNewTextBlock());
  }

  public addProductBlock() {
    this.props.dispatch(addNewProductBlock());
  }

  public render() {
    return (
      <div className="c-add-content-block">
        <div className="c-add-content-block__top-section">
          <h3>Add Content</h3>
        </div>
        <div className="c-add-content-block__buttons">
          <button className="c-btn c-btn--primary c-add-content-block__button" onClick={this.addTextBlock}>
            Text
          </button>
          <button className="c-btn c-btn--primary c-add-content-block__button" onClick={this.addProductBlock}>
            Product
          </button>
        </div>
      </div>
    );
  }
}

export default connect()(AddContentBlock);
