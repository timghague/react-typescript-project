import { Reducer } from 'redux';

import { ArticleState } from '../app-state';
import { actionTypes } from '../action-types';
import { ArticleAction } from '../actions/article-actions';

import { getNewTextBlock, isBlockText } from '../models/block-text';
import { getNewProductBlock, isBlockProduct } from '../models/block-product';

export const articleReducer: Reducer<ArticleState> =
  (
    state: ArticleState = {
      currentArticle: {
        id: 0,
        items: []
      },
      editArticleIndex: false
    },
    action: (ArticleAction)
  ) => {

    switch (action.type) {
      case actionTypes.SET_ARTICLE:
        return Object.assign({}, state, {currentArticle: action.article});
      case actionTypes.ADD_NEW_TEXT_BLOCK:
        const articleToAddTextBlock = Object.assign({}, state.currentArticle);
        articleToAddTextBlock.items.push(getNewTextBlock());
        return Object.assign({}, state, {currentArticle: articleToAddTextBlock});
      case actionTypes.ADD_NEW_PRODUCT_BLOCK:
        const articleToAddProductBlock = Object.assign({}, state.currentArticle);
        articleToAddProductBlock.items.push(getNewProductBlock());
        return Object.assign({}, state, {currentArticle: articleToAddProductBlock});
      case actionTypes.SET_ARTICLE_ITEM_TO_EDIT:
        return Object.assign({}, state, {
          editArticleIndex: action.articleItemIndex,
          editArticleItem: action.articleItem
        });
      case actionTypes.DELETE_BLOCK:
        if (typeof action.articleItemIndex === 'number') {
          let articleDelete = Object.assign({}, state.currentArticle);
          articleDelete.items = articleDelete.items.filter((item, index) => index !== action.articleItemIndex);

          // If editing article to delete, clear edit article.
          let editArticleIndex = state.editArticleIndex;
          if (state.editArticleIndex === action.articleItemIndex) {
            editArticleIndex = false;
          }

          return Object.assign({}, state, {
            currentArticle: articleDelete,
            editArticleIndex
          });
        }
        return state;
      case actionTypes.UPDATE_EDITED_ARTICLE:
        const updatedArticleItem = action.articleItem;
        const updatedArticleItems = state.currentArticle.items.map((articleItem, index) => {
          if (
            index === state.editArticleIndex &&
            (
              isBlockText(updatedArticleItem) ||
              isBlockProduct(updatedArticleItem)
            )
          ) {
            return updatedArticleItem;
          }
          return articleItem;
        });

        let updatedArticle = Object.assign({}, state.currentArticle);
        updatedArticle.items = updatedArticleItems;

        return Object.assign({}, state, {
          currentArticle: updatedArticle,
          editArticleIndex: false,
        });
      case actionTypes.CANCEL_EDIT_ARTICLE:
        return Object.assign({}, state, {
          editArticleIndex: false,
        });
      case actionTypes.CLEAR_ARTICLE:
        return Object.assign({}, state, {currentArticle: {id: 0, content: []}});
      default:
        return state;
  }
};
