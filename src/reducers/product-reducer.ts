import { Reducer } from 'redux';

import { ProductState } from '../app-state';
import { actionTypes } from '../action-types';
import { ProductAction } from '../actions/product-actions';

export const productReducer: Reducer<ProductState> =
  (
    state: ProductState = {
      allProducts: [],
    },
    action: (ProductAction)
  ) => {

    switch (action.type) {
      case actionTypes.SET_PRODUCTS:
        return Object.assign({}, state, {allProducts: action.products});
      case actionTypes.CLEAR_PRODUCTS:
        return Object.assign({}, state, {allProducts: []});
      default:
        return state;
  }
};
