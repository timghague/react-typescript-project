import { combineReducers } from 'redux';
import { articleReducer } from './article-reducer';
import { productReducer } from './product-reducer';

export default combineReducers({
  article: articleReducer,
  product: productReducer
});
